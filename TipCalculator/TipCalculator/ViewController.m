//
//  ViewController.m
//  TipCalculator
//
//  Created by Rohit Singh on 31/08/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize label,textField,slider,curreentTipLabel,tipLabel;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a
    
    
    NSString *tcurrentperc=[[NSString alloc]initWithFormat:@"%1.2f%%",[slider value]*100];
    
    [curreentTipLabel setText:tcurrentperc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonPressed:(id)sender {
   
    NSString *input=[textField text];
    if([input length]==0){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Bill Amount" message:@"You forgot to enter Bill Amount" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [label setText:@""];
        [tipLabel setText:@""];
        [alert show];
    }
    else{
    
    float billAmount=[input floatValue];
    float tipamount=[slider value]*billAmount;
        float totalamount=tipamount+billAmount;
    NSString *tipString=[[NSString alloc]initWithFormat:@"%1.2f",tipamount];
        NSString *tolString=[[NSString alloc]initWithFormat:@"%1.2f",totalamount];
    [tipLabel setText:tipString];
        [label setText:tolString];
        [self.view endEditing:YES];
        
    }
}

- (IBAction)sliderValueChanged:(id)sender {
   
    NSString *tcurrentperc=[[NSString alloc]initWithFormat:@"%1.2f%%",[slider value]*100];
    
    [curreentTipLabel setText:tcurrentperc];
}
@end
