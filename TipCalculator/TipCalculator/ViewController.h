//
//  ViewController.h
//  TipCalculator
//
//  Created by Rohit Singh on 31/08/2014.
//  Copyright (c) 2014 ROHIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *label;
- (IBAction)buttonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *tipLabel;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UILabel *curreentTipLabel;
- (IBAction)sliderValueChanged:(id)sender;


@end
